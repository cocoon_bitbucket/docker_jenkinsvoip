__author__ = 'cocoon'
"""

    simulate a robot framework client

"""

from syprunner.robot_plugin import Pilot
from syprunner.syprfc import create_header_from_line



        # from config.yaml
        # # base
        # base: BTIC_syprun
        # # directory for script output
        # gen_dir: "../../../scripts/BTIC_syprun"
        # # media files path
        # media: ../../media
        # #####################################
        # # runner ( SypRunner or EasyInjector)
        # runner: "SypRunner"
        # # target base directory for runner ()
        # runner_dir: /mnt/hgfs/wheezyvalid


syp_ptf=dict(




    main = {
      # platform name
      'platform_name': 'DEMO',
      'platform_version': 'GEN',

      'platform_instance': "demo_gen",

      'platform_dir': '/home/cocoon/apps',
      # root: /mnt/hgfs/Auto/demo
      #
      # proxy (SBC) for register :ip:port
      'proxy': 'bluebox:5060',
      # local port policy: 0 : for auto select userA:5061 , userB:5062 ....
      #'registrar' : "bluebox",

      #                    or a fixed port eg 5060
      'local_port_policy': 0,
      #
      # default password and domain for users
      'password': 'cocoon',
      'domain': 'bluebox',
      #
      # server broadsoft
      'ApplicationServerUrl': "http://bluebox/bluebox",

      # easyrunner
      # generate scripts for syprunner
      'engine': "syprunner",
      #engine: "SypRunner"

      # platform profile : default , ims
      'profile': 'default',

    },

    destinations = {

      # Speed Dial 8
      'Alice_SD_2': 2,  # Bob
      'Alice_SD_3': 3,   # Bob_Intersite
      'Alice_SD_4': 4,   # Bob_InterEnterprise
      'Alice_noSDA_SD_2': 2,   # Bob
      'Alice_noSDA_SD_3': 3,   # Bob_Intersite
      'Alice_noSDA_SD_4': 4,   # Bob_InterEnterprise

      'wrong_Sid_E1S1': '112527',
      'wrong_Sid_E1S2': '122518',
      'wrong_Extension': '00000',
      'wrong_PrivateNumber': '112527',

      'unallocatedNumber_1': 113333,
      #   - 113333
      #   - 122222
      #   - 00145444444
      #   - 2517456
      #   - 112190

      'wrong_FeatureAccessCode_1': '*100',
      'wrong_FeatureAccessCode_2': '*72',

    },

    enterprises=  dict(
        enterprise_1 = dict(
            name = "LPRISMEVQ1",
            CID = 0,
            sites = dict(
                site_1= dict (
                  name = "2361EVQ1",
                  LocationDialingCode = '11',
                  CallingLineIDGroupNumber = '0146502512',
                  CallingLineIDGroupName = "CLID_2361EVQ1",
                  users= dict(
                    Alice=             {'username': '+33146502510' ,'domain': 'bluebox', 'password': 'cocoon'},
                    Bob=               {'username': '+33146502511'},
                    Charlie=           {'username': '+33146502512'},
                    Dave=              {'username': '+33146502513'},
                    Erin=              {'username': '+33146502514'},
                    Alice_noSDA=       {'username': 'P2361EVQ1_6310' , 'number': '6310'},
                    Bob_noSDA =        {'username': 'P2361EVQ1_6311' , 'number': '6311' },
                    Bob_noSDA_Short =  {'username': 'P2361EVQ1_6312' , 'number': '6312', 'target': 'Bob_Short_Target'},
                    Bob_Short_Target = {'username': '+33146502523'},
                  ),
                ),
                site_2 = dict(
                  name = "2361EVQ2",
                  LocationDialingCode = '12',
                  CallingLineIDGroupNumber = '0146502519',
                  CallingLineIDGroupName = 'CLID_2361EVQ2',
                  users = dict(
                    Alice_InterSite =       {'username': '+33146502520'},
                    Bob_InterSite =         {'username': '+33146502521'},
                    Charlie_InterSite =     {'username': '+33146502522'},
                    Dave_InterSite =        {'username': '+33146502523'},
                    Alice_InterSite_noSDA = {'username': 'P2361EVQ2_6321', 'number': '6321'},
                    Bob_InterSite_noSDA =   {'username': 'P2361EVQ2_6320', 'number': '6320'},
                  ),
                ),
                virtual = dict(
                  name = "virtual",
                  users = dict(
                      Bob_Abbreviated = { 'virtual': 'true' , 'username': '3212', 'target': 'Bob_Abbreviated_Target' },
                  ),
                ),
            ),
        ),
        enterprise_2 = dict(
            name = "LPRISMEVQ2",
            CID = 0,
            sites = dict(
              site_1 = dict(
                name = "2361EVQ3",
                LocationDialingCode = '21',
                CallingLineIDGroupNumber = '0146502533',
                CallingLineIDGroupName = 'CLID_2361EVQ3',
                users = dict(
                  Alice_InterEnterprise =       {'username': '+33146502531'},
                  Bob_InterEnterprise =         {'username': '+33146502532'},
                  Bob_Abbreviated_Target =      {'username': '+33146502533'},
                  Alice_InterEnterprise_noSDA = {'username': 'P2361EVQ3_6330'},
                  Bob_InterEnterprise_noSDA =   {'username': 'P2361EVQ3_6331'},
                ),
              ),
            ),
        ),
    ),

FeatureAccessCode = {
"+DVMT": ["*55", "Direct Voice Mail Transfer"], "+CFA": ["*21", "CFA Activation Call Forward Always"], "+CFB": ["*22", "CFB Activation , CallForward Busy Activation"], "-CFS": ["*271*", "Call Forwarding Selective Activation"], "+ExAOin": ["#65", "Executive-Assistant Opt-in"], "-CLIR": ["#3651", "Call Line ID Delivery overiding , per call when permanent Line Identity Restriction"], "+DND": ["*78", "Do Not Disturb Activation"], "-DND": ["*79", "Do Not Disturb DeActivation"], "+CFS": ["*270*", "Call Forwarding Selective Activation"], "-CFB": ["*221*", "CFB DeActivation , CallForward Busy DeActivation"], "-CFA": ["*211*", "Call Forward Always Deactivation"], "-CWP": ["*431*", "Call Waiting Persistent Deactivation"], "+CFNA_VM": ["*23*", "CFNA-VM Activation , Call Forward No Answer to Voice Mail Activation"], "+LC": ["*12", "Location Control activation"], "-LC": ["*13", "Location Control deactivation"], "+ExACP": ["#63", "Executive-Assistant Call Push"], "-CFB_VM": ["*223*", "CFB-VM  DeActivation , CallForward Busy to Voice Mail DeActivation"], "?CFA": ["*212*", "Call Forward Always Interrogation"], "?CFB": ["*222*", "CFB Interrogation , CallForward Busy Interrogation"], "+SD100": ["*75", "Speed Dial 100"], "+ExAIC": ["#64", "Executive-Assistant Initiate Call"], "?CWP": ["*53*", "Call Waiting Interrogation"], "+CB": ["*15", "CAll Bridge"], "+CFA_VM": ["*21*", "CFA-VM Activation , CAll Forward Always to Voice Mail Activation"], "+DCPup": ["*41", "Directed Call Pickup"], "+CPup": ["*41*", "Call Pickup"], "+CLIR": ["*3651", "Call Line ID Delivery Blocking , per call, or Call Line Identity Restriction"], "-CFNA_VM": ["*233*", "CFNA-VM DeActivation , Call Forward No Answer to Voice Mail DeActivation"], "+CPR": ["*88", "Call Park Retrieve"], "+CP": ["*62", "Call Park"], "+CR": ["*31", "Call Return activation"], "-CR": ["*311*", "Call Return deactivation (Number Deletion)"], "+ACR": ["*77", "Anonymous Call Rejection activation"], "+CFB_VM": ["*22*", "CFB-VM  Activation , CallForward Busy to Voice Mail Activation"], "+BAE164D": ["*14", "Broadworks Anywhere E.164 Dialing"], "-ACR": ["*87", "Anonymous Call Rejection deactivation"], "-CFNR": ["*241*", "CFNR DeActivation,    Call Forward Not Reachable DeActivation"], "+SD8": ["*74", "Speed Dial 8"], "+CWP": ["*43*", "Call Waiting Persistent Activation"], "+LNR": ["*33", "Last Number Redial"], "+CFNA": ["*23", "CFNA Activation , Call Forward No Answer Activation"], "+CFNR": ["*24", "CFNR Activation,    Call Forward Not Reachable Activation"], "?ACR": ["*52*", "Anonymous Call Rejection Interrogation"], "-CFNA": ["*231*", "CFNA DeActivation , Call Forward No Answer DeActivation"], "?CFNR": ["*242*", "CFNR Interrogation,    Call Forward Not Reachable Interrogation"], "-CFA_VM": ["*213*", "CFA-VM DeActivation , CAll Forward Always to Voice Mail DeActivation"], "+ExAOout": ["#66", "Executive-Assistant Opt-out"], "?CFNA": ["*232*", "CFNA Interrogation , Call Forward No Answer Interrogation"]
}


)

class SypRfDemo(Pilot):
    """
        custom pilot for demo platform

    """
    _platform = syp_ptf
    _dry = False


if __name__ == "__main__":



    from syprunner.ptf_manager import SypPlatform


    ptf= SypPlatform(syp_ptf)

    alice = ptf.user('Alice')

    print alice.to_user('universal')

    print ptf.registrar()
    print ptf.proxy()

    target1 = ptf.destination('unallocatedNumber_1')


    cfa_activate = ptf.fac('+CFA')
    print cfa_activate







    user_conf= [

    dict( display="Alice", username="+33146502510", domain="bluebox",password="cocoon",proxy="sip:bluebox:5060",registrar="sip:bluebox"  ),
    dict( display="Bob", username="+33146502511", domain="bluebox",password="cocoon",proxy="sip:bluebox:5060",registrar="sip:bluebox"  ),

    ]



    def simple_call(pilot):
        """



        :param pilot:
        :return:
        """
        #
        pilot.open_Session('Alice','Bob')

        pilot.call_user("Alice","Bob",'ext')


        from_line = pilot.wait_incoming_call("Bob")
        res = pilot.check_incoming_call("Bob",from_user="Alice")

        # from_line = create_header_from_line(from_line)
        # print from_line.tag
        # print from_line.value.uri.user
        # print from_line.value.uri.host
        # print from_line.value.displayable
        # from_line sample:  From: "2510" <sip:2510@172.16.229.144>;tag=j8Dy38Dg5U29Q

        pilot.answer_call_ok("Bob")
        pilot.wait_call_confirmed("Alice")

        pilot.sleep(5)


        pilot.hangup("Alice")

        return






    def call_with_media(pilot):
        """

            Alice Call Bob , Bob answer , Bob PLAy file

        """
        #
        pilot.open_Session('Alice','Bob')


        pilot.call_user("Alice","Bob",'ext')

        #pilot.wait_incoming_call_and_answer_ok("Bob")

        from_line = pilot.wait_incoming_call("Bob")
        #res = pilot.check_incoming_call("Bob",from_user="Alice")

        #from_line = create_header_from_line(from_line)
        # print from_line.tag
        # print from_line.value.uri.user
        # print from_line.value.uri.host
        # print from_line.value.displayable
        # from_line sample:  From: "2510" <sip:2510@172.16.229.144>;tag=j8Dy38Dg5U29Q


        pilot.answer_call_ok("Bob")

        pilot.wait_call_confirmed("Alice")

        pilot.start_player('Bob')
        pilot.watch_log('Bob',  2)
        pilot.watch_log('Alice',2)

        #pilot.dump_call_quality_status('Alice')


        pilot.stop_player('Bob')
        pilot.watch_log('Bob' , 1)
        pilot.watch_log('Alice',1)


        #pilot.dump_call_quality_status('Alice')
        stats = pilot.check_call_quality('Alice',min_packets=10,max_loss_rate=0.01,slot='0',channel='TX',call=None)


        #pilot.dump_call_quality_status('Bob')
        stats = pilot.check_call_quality('Bob',min_packets=100,max_loss_rate=0.01,slot='0',channel='TX',call=None)



        pilot.hangup("Alice")

        return

    def hold_reinvite(pilot):
        """

        """

        pilot.open_Session('Alice','Bob')
        pilot.call_user('Alice', 'Bob', 'sid_ext')
        from_line = pilot.wait_incoming_call('Bob')
        pilot.answer_call_ok('Bob')

        response= pilot.wait_incoming_response('Alice','200','INVITE')

        pilot.wait_call_confirmed('Alice')
        #sleep    1
        pilot.hold_call('Alice')
        pilot.watch_log('Bob',1)
        pilot.watch_log('Alice',1)

        pilot.unhold_call('Alice')
        pilot.watch_log('Bob',1)
        pilot.watch_log('Alice',1)

        pilot.hangup('Alice')

        return


    def blind_transfer(pilot):
        """

        """

        pilot.open_Session('Alice','Bob','Bob_InterSite')
        pilot.call_user('Alice','Bob','sid_ext')
        from_line = pilot.wait_incoming_call('Bob')
        pilot.answer_call_ok('Bob')
        pilot.wait_call_confirmed('Alice')
        #sleep    1
        pilot.transfer_to_user('Bob','Bob_InterSite','sid_ext')
        from_line = pilot.wait_incoming_call('Bob_InterSite')
        pilot.answer_call_ok('Bob_InterSite')
        #wait_call_confirmed    ${userA}
        pilot.watch_log('Bob_InterSite',2)
        pilot.hangup('Bob_InterSite')
        pilot.wait_hangup('Alice')
        return


    def attended_transfer(pilot):
        """

        """
        pilot.open_Session('Alice','Bob','Bob_InterSite')

        # A call B
        pilot.call_user('Alice','Bob','sid_ext')
        from_line = pilot.wait_incoming_call('Bob')
        pilot.answer_call_ok('Bob')
        pilot.wait_call_confirmed('Alice')
        # call established

        # B hold A
        pilot.hold_call('Bob')


        # B call C
        pilot.call_user('Bob','Bob_InterSite','sid_ext')
        from_line = pilot.wait_incoming_call('Bob_InterSite')
        pilot.answer_call_ok('Bob_InterSite')
        pilot.wait_call_confirmed('Bob')
        # call established

        # B hold C
        pilot.hold_call('Bob')


        # B transfer call with C to A
        pilot.transfer_attended('Bob',0)

        pilot.watch_log('Bob',2)

        pilot.watch_log('Alice',2)

        pilot.watch_log('Bob_InterSite',2)


        pilot.hangup('Alice')



        return




    # starts here
    pilot = SypRfDemo()

    pilot.setup_pilot('demo','demo_qualif', "./platform.json")


    print pilot.get_sip_address('Alice','ext',fac="+CFA")
    print pilot.get_sip_address('Alice','international')


    #simple_call(pilot)

    call_with_media(pilot)

    #hold_reinvite(pilot)

    #blind_transfer(pilot)

    #attended_transfer(pilot)


    pilot.close_session()



    pass